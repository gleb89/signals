FROM node:17-alpine

WORKDIR /app

# Install app dependencies
RUN apk update && apk upgrade && apk add git
# RUN rm -rf node_modules package-lock.json && npm install

COPY . /app
RUN npm install

# Build app
# RUN npm run build

ENV HOST 0.0.0.0
EXPOSE 3001



CMD [ "node", "index.js" ]